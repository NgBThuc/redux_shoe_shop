import React, { Component } from "react";
import { connect } from "react-redux";
import ShoeItem from "./ShoeItem";

class ShoesList extends Component {
  render() {
    let shoesArray = this.props.shoesArray;

    return (
      <div className="py-10">
        <h1 className="text-2xl font-bold">Shoes Products</h1>

        <div className="py-5 grid grid-cols-3 gap-4">
          {shoesArray.map((shoe) => (
            <ShoeItem key={shoe.id} shoe={shoe} />
          ))}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return { shoesArray: state.shoeReducer.shoesArray };
};

export default connect(mapStateToProps)(ShoesList);
