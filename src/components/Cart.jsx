import React, { Component } from "react";
import { connect } from "react-redux";
import { HIDE_CART } from "../redux/constants/shoeConstants";
import CartItem from "./CartItem";

class Cart extends Component {
  render() {
    let isCartVisible = this.props.isCartVisible;
    let cart = this.props.cart;

    const countTotal = () => {
      let total = 0;
      cart.map((cartItem) => (total += cartItem.quantity * cartItem.price));
      return new Intl.NumberFormat("vn-VN").format(total);
    };

    return (
      <div
        className={`${
          isCartVisible ? "" : "translate-x-full"
        } fixed p-8 right-0 top-0 h-screen w-[50vw] bg-wild-sand text-cod-gray transition-all overflow-y-scroll`}
      >
        <div>
          <i
            onClick={this.props.handleHideCart}
            className="text-3xl fa fa-times-circle transition-colors cursor-pointer hover:text-brown"
          ></i>
        </div>
        <h3 className="text-3xl text-center font-bold text-brown mb-8">Cart</h3>

        {cart.length === 0 ? (
          <>
            <p className="text-lg font-bold text-cod-gray text-center">
              You don't have any item in your cart yet
            </p>
          </>
        ) : (
          <>
            <div className="flex flex-col gap-2 border-b-2 border-nobel pb-10">
              {cart.map((cartItem) => {
                return (
                  <CartItem key={cartItem.id} cartItem={{ ...cartItem }} />
                );
              })}
            </div>
            <div className="pt-10 flex items-center justify-center w-full gap-2">
              <p className="text-2xl font-semibold text-cod-gray">Total:</p>
              <p className="text-2xl font-semibold text-cod-gray">
                {countTotal()}$
              </p>
            </div>
          </>
        )}
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    isCartVisible: state.shoeReducer.isCartVisible,
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleHideCart: () => {
      let action = {
        type: HIDE_CART,
      };
      dispatch(action);
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Cart);
