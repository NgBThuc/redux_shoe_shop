import { dataArray } from "../../data/shoesData";
import {
  ADD_TO_CART,
  CLOSE_SHOE_DETAIL,
  DECREASE_QUANTITY,
  DELETE_CART_ITEM,
  HIDE_CART,
  INCREASE_QUANTITY,
  SHOW_CART,
  SHOW_SHOE_DETAIL,
} from "../constants/shoeConstants";

const initialState = {
  shoesArray: dataArray,
  shoeDetail: {},
  cart: [],
  isShoeDetailVisible: false,
  isCartVisible: false,
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      let cart = [...state.cart];
      let cartItem = { ...payload, quantity: 1 };
      let index = cart.findIndex((cartItem) => cartItem.id === payload.id);

      if (index === -1) {
        cart.push(cartItem);
      } else {
        cart[index].quantity++;
      }

      state.cart = cart;
      return { ...state };
    }
    case SHOW_SHOE_DETAIL: {
      state.shoeDetail = payload;
      state.isShoeDetailVisible = true;
      return { ...state };
    }
    case CLOSE_SHOE_DETAIL: {
      state.isShoeDetailVisible = false;
      return { ...state };
    }
    case HIDE_CART: {
      state.isCartVisible = false;
      return { ...state };
    }
    case INCREASE_QUANTITY: {
      let cart = [...state.cart];
      let index = cart.findIndex((cartItem) => cartItem.id === payload);
      cart[index].quantity++;
      state.cart = cart;
      return { ...state };
    }
    case DECREASE_QUANTITY: {
      let cart = [...state.cart];
      let index = cart.findIndex((cartItem) => cartItem.id === payload);
      if (cart[index].quantity === 1) {
        return;
      }
      cart[index].quantity--;
      state.cart = cart;
      return { ...state };
    }
    case DELETE_CART_ITEM: {
      let cart = [...state.cart];
      cart = cart.filter((cartItem) => cartItem.id !== payload);
      state.cart = cart;
      return { ...state };
    }
    case SHOW_CART: {
      state.isCartVisible = true;
      return { ...state };
    }
    default:
      return state;
  }
};
