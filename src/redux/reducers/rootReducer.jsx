import { combineReducers } from "redux";
import { shoeReducer } from "./shoeReducer";

export let rootReducer_ShoeShop = combineReducers({
  shoeReducer: shoeReducer,
});
