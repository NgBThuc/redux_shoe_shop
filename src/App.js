import React, { Component } from "react";
import "./App.css";
import Cart from "./components/Cart";
import Header from "./components/Header";
import ShoeDetailPopup from "./components/ShoeDetailPopup";
import ShoesList from "./components/ShoesList";

export default class App extends Component {
  render() {
    return (
      <div className="container mx-auto py-5 relative">
        <Header />
        <ShoesList />
        <ShoeDetailPopup />
        <Cart />
      </div>
    );
  }
}
